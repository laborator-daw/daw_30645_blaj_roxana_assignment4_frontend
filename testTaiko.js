const { openBrowser, goto, click, write, button, dropDown, clear, closeBrowser } = require('taiko');
(async () => {
    try {
        await openBrowser();
        await goto("localhost:3000");
        await goto("localhost:3000/login");
        await goto("localhost:3000/register");
        await click("Name");
        await write("taiko");
        await click("Email");
        await write("taiko@test.com");
        await click("Password");
        await write("12345");
        await click("Confirm Password");
        await write('12345');
        await click("Submit");
        await click("Email");
        await write("taiko@test.com");
        await click("Password");
        await write("12345");
        await click("LOGIN");
        await click("login");
        await click(button("login"));
        await click("Medical services");
        await click('Name');
        await click('Medical sevices:');
        await dropDown('Medical sevices:');
        await dropDown('Medical sevices:').select('Albire');
        await click('Modify user data');
        await click('Password:');
        await write('123456');
        await clear();
        await write('123456');
        await click(button("change password"));
        await click("Delogare");
        await click("email");
        await write("taiko@test.com");
        await click("Password");
        await write("12345");
        await click("password");
        await write('123456');
        await click(button('login'));
        await click('Programare');
        await click('Select appointment date');
        await click("select hour");
        await click("select hour:");
        await dropDown("select hour:");
        await click("select doctor:");
        await dropDown("select doctor:").select("Dr. John Doe");
        await dropDown("select hour:").select("10:00");
        await dropDown("Select medical service:").select("Consultatie");
        await click("Delogare");
        await click("email");
        await write("johnDoe@medic.com");
        await click("Password");
        await write("12345");
        await click(button("login"));
        await click("statistica");
        await goto("localhost:3000/profile");


        ;
	;
	;
    } catch (error) {
        console.error(error);
    } finally {
        await closeBrowser();
    }
})();

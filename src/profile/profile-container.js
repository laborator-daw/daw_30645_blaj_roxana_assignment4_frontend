import React from 'react';
import "../commons/css/profile.css"
import validate from "../commons/validators/validators";

import APIResponseErrorMessage from "../commons/error-handeling/api-response-error-message";
import {
    FormGroup, Input, Label, Row, Col, Button
} from 'reactstrap';
import axios from "axios";


const input_style = {
    lineHeight: 1.5,
    color: '#666666',
    display: 'block',
    width: '100%',
    background: '#e6e6e6',
    height: '50px',
    borderRadius: '25px',
    padding: '0 30px 0 68px',
    marginBottom: '30px',
};

class ProfileContainer extends React.Component {

    constructor(props) {

        super(props);
        this.handleChange=this.handleChange.bind(this);
        this.handlePass = this.handlePass.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangePass = this.handleChangePass.bind(this);
        this.handleChangeSelectedData = this.handleChangeSelectedData.bind(this);
        this.handleChangeFile = this.handleChangeFile.bind(this);

        this.state = {

            errorStatus: 0,
            error: null,
            err: false,
            errorMessage: '',
            formIsValid: false,
            valueSM: '',
            valueAllServMed: '',
            gdprFile: '',
            selectedFile: null,
            formControls: {

                name: {
                    value: '',
                    valid: false,
                    touched: false,
                    validationRules: {
                        isRequired: true,
                        minLength: 5
                    }
                },
                email: {
                    value: '',
                    valid: false,
                    touched: false,
                    validationRules: {
                        emailValidator: true
                    }
                },
                password: {
                    value: '',
                    valid: false,
                    touched: false,
                    validationRules: {
                        isRequired: true,
                        minLength: 6
                    }
                },
            }
        };
        this.getUserData = this.getUserData.bind(this);
        this.getUserData();
    }

    getUserData() {
        let idUser = localStorage.getItem("id");

        axios.get(
            "http://127.0.0.1:8000/api/userdata/" + idUser,
        ).then((response) => {
            this.setState({
                valueAllServMed: response.data.servMed,
                gdprFile:response.data.gdprFile,
                formControls:{
                    name:{
                        value: response.data.name
                    },
                    email:{
                        value: response.data.email
                    },
                    password:{
                        value: response.data.password
                    },
                }
            });

        });
    };

    handleChangeSelectedData = event => {
        const name = event.target.name;
        const value = event.target.value;

        this.setState({
            valueSM: value
        });
    };

    handleChangeFile = e => {
        let files = e.target.files;
        this.setState({
            selectedFile: files[0]
        })

    };

    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });

    };


    handlePass = event => {
        const value = event.target.value;
        const name = event.target.name;
        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];
        updatedFormElement.value = value;
        updatedFormElement.touched = true;

        let formIsValid = true;
        formIsValid = this.state.formControls.password.value === value;

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });

    };

    handleSubmit() {
        let userId = localStorage.getItem("id");
        let user = {
            name: this.state.formControls.name.value,
            email: this.state.formControls.email.value,
            password: this.state.formControls.password.value,
            role: 'patient',
            servMed: this.state.valueSM,
            gdprFile: null
        };

        axios.put(
            "http://127.0.0.1:8000/api/userdata/" + userId,
            user
        )
            .then((response) => {
                //console.log(response.status);
                if (response.status === 200) {
                    this.setState({
                        valueAllServMed: response.data.servMed
                    });
                    this.getUserData();
                }
                if (response.data.status === "failed" || response.status === 500) {
                    console.log(response.data.message);
                    alert("Data is invalid or email is already used. Try again!");
                    this.getUserData();
                }
            });

        const data = new FormData()
        data.append('gdprFile', this.state.selectedFile)

        let url = "http://127.0.0.1:8000/api/updateFile/" + userId;
        console.log(url);

        axios.post(url, data )
        .then((response) => {

            if (response.status === 200) {
                this.setState({
                    gdprFile: response.data.gdprFile
                });
            }
        });



    };

    handleChangePass() {
        let userId = localStorage.getItem("id");
        let password =  {
            password: this.state.formControls.password.value
        };

        axios.put(
            "http://127.0.0.1:8000/api/updatePassword/" + userId,
            password
        )
            .then((response) => {
                console.log(response.status);
                if (response.status === 200) {
                    this.getUserData();
                }
                if (response.data.status === "failed" || response.status === 500) {
                    console.log(response.data.message);
                    alert("Something went wrong. Try again!");
                    this.getUserData();
                }
            });
    };

    render() {

        return (
            <div className="profile-page">
                <div className="container-profile100">
                    <div className="wrap-profile100">
                         <span className="profile100-form-title">
                                User Profile
                         </span>

                            <div className="profile-inputs">
                                <FormGroup className="profile-input" id='name'>
                                    <Label className='txt1' for='nameField'> Name: </Label>
                                    <Input style={input_style} name='name' id='nameField'
                                           onChange={this.handleChange}
                                           defaultValue={this.state.formControls.name.value}
                                           touched={this.state.formControls.name.touched ? 1 : 0}
                                           valid={this.state.formControls.name.valid}
                                           required
                                    />

                                </FormGroup>
                                <br/><br/>

                                <FormGroup className="profile-input" id='email'>
                                    <Label className='txt1' for='emailField'> Email: </Label>
                                    <Input style={input_style} name='email' id='emailField'
                                           onChange={this.handleChange}
                                           defaultValue={this.state.formControls.email.value}
                                           touched={this.state.formControls.email.touched ? 1 : 0}
                                           valid={this.state.formControls.email.valid}
                                           required
                                    />

                                </FormGroup>
                                <br/><br/>

                                <FormGroup className="profile-input" id='password'>
                                    <Label className='txt1' for='passwordField'> Password: </Label>
                                    <Input style={input_style} type='password' name='password' id='passwordField'
                                           onChange={this.handleChange}
                                           defaultValue={this.state.formControls.password.value}
                                           touched={this.state.formControls.password.touched ? 1 : 0}
                                           valid={this.state.formControls.password.valid}
                                           required
                                    />

                                </FormGroup>
                                <br/><br/>

                                <FormGroup className="profile-input" id='medServ'>
                                    <Label className='txt1' for='medServField'> Medical sevices: </Label>
                                    <Input value={this.state.valueSM} onChange={this.handleChangeSelectedData} style={input_style} type="select" name="medServ" id="medServField" >
                                        <option value="0" hidden>Choose medical services...</option>
                                        <option value="Consultatie">Consultatie</option>
                                        <option value="Detartraj">Detartraj </option>
                                        <option value="Periaj profesional">Periaj profesional</option>
                                        <option value="Sigilare santuri">Sigilare santuri</option>
                                        <option value="Obturatii ">Obturatii</option>
                                        <option value="Albire">Albire  </option>
                                        <option value="Gutiere de albire">Gutiere de albire </option>
                                        <option value="Indepartat punte">Indepartat punte </option>
                                        <option value="Coroana metalica Cr-Ni">Coroana metalica Cr-Ni </option>
                                        <option value="Coroana provizorie acrilica">Coroana provizorie acrilica</option>
                                        <option value="Coroana integral ceramica">Coroana integral ceramica</option>
                                        <option value="Proteza">Proteza  </option>
                                        <option value="Fatete">Fatete </option>
                                    </Input>

                                </FormGroup>
                                <br/><br/>

                                <p style={{marginTop: '20px'}}> Medical services now: {this.state.valueAllServMed} </p>

                                <br/><br/>

                                <FormGroup className="profile-input" id='gdpr'>
                                    <Label className='txt1' for='gdpr'> Update GDPR approval: </Label>
                                    <br/>
                                    <input  type='file' name='gdpr' onChange={this.handleChangeFile} />
                                    <br/>
                                    <p> Current GDPR FILE:{this.state.gdprFile}</p>

                                </FormGroup>
                                <br/><br/><br/><br/>


                                <Row>
                                    <Col sm={{size: '4', offset: 6}}>
                                        <Button type="button" className='profile100-form-btn' onClick={this.handleSubmit}> Modify user data </Button>
                                    </Col>
                                </Row>
                                <br/>
                                <Row>
                                    <Col sm={{size: '4', offset: 6}}>
                                        <Button type="button" className='profile100-form-btn' onClick={this.handleChangePass}> Change password </Button>
                                    </Col>
                                </Row>

                            </div>


                        </div>
                </div>
            </div>
        )
    }
}

export default ProfileContainer;

import React from 'react';
import {FormGroup, Label, Input, Row, Col, Button, CardHeader} from 'reactstrap';
import axios from "axios";
import "../commons/css/register.css"
import '../commons/fonts/font-awesome-4.7.0/css/font-awesome.min.css'

import validate from "../commons/validators/validators";
import Img from "../commons/images/createAccount.jpg";

const registerCardHeader = {
    backgroundColor: "#FFFFFF",
    fontSize:'25px',
    textAlign:'center',

    marginBottom:'20px'
};


const input_style = {
    lineHeight: 1.5,
    color: '#666666',
    display: 'block',
    width: '100%',
    background: '#e6e6e6',
    height: '50px',
    borderRadius: '25px',
    padding: '0 30px 0 68px',
};

const input_style_password = {
    lineHeight: 1.5,
    color: '#666666',
    display: 'block',
    width: '100%',
    background: '#e6e6e6',
    height: '50px',
    borderRadius: '25px',
    marginTop: '10px',
    padding: '0 30px 0 68px',
};




class RegisterContainer extends React.Component {

    constructor(props) {

        super(props);
        this.handleChange=this.handleChange.bind(this);
        this.handlePass = this.handlePass.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

        this.state = {
            users: [],
            errorStatus: 0,
            error: null,
            err: false,
            errorMessage: '',

            formIsValid: false,

            formControls: {
                name: {
                    value: '',
                    placeholder: 'Name...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        isRequired: true
                    }
                },
                email: {
                    value: '',
                    placeholder: 'Email...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        emailValidator: true
                    }
                },
                password: {
                    value: '',
                    placeholder: 'Password...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        isRequired: true,
                        minLength: 6
                    }
                },

                confirmpass: {
                    value: '',
                    placeholder: 'Confirm Password...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        isRequired: true,
                    }
                },

            }
        };


    }

    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });

    };

    handlePass = event => {
        const value = event.target.value;
        const name = event.target.name;
        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];
        updatedFormElement.value = value;
        updatedFormElement.touched = true;

        let formIsValid = true;
        formIsValid = this.state.formControls.password.value === value;

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });

    };

    handleSubmit(e) {

        let user = {
            name: this.state.formControls.name.value,
            email: this.state.formControls.email.value,
            password: this.state.formControls.password.value,
            role: 'patient',
            servMed: null,
            gdprFile: null
        };
        axios.post(
            "http://127.0.0.1:8000/api/userdata",
            user
        )
            .then((response) => {
                console.log(response);
                if (response.data.status === "created") {
                    console.log(response.data.message);
                    window.location.href="/login";
                }
                if (response.data.status === "failed") {
                    console.log(response.data.message);
                    alert("Data is invalid or email is already used. Try again!");

                }
            });
    }

    render() {
        return (
            <div className="register-page">
                <div className="container-register100">
                    <div className="wrap-register100">
                        <div className="register100-pic" >
                            <img src={Img} alt="IMG"/>
                        </div>

                        <div className="register-inputs">
                            <CardHeader style={registerCardHeader}>
                                <strong> Register </strong>
                            </CardHeader>

                            <FormGroup className="register-input" id='email'>
                                <Label for='nameField'> Name: </Label>
                                <Input style={input_style} name='name' id='nameField' placeholder={this.state.formControls.name.placeholder}
                                       onChange={this.handleChange}
                                       defaultValue={this.state.formControls.name.value}
                                       touched={this.state.formControls.name.touched ? 1 : 0}
                                       valid={this.state.formControls.name.valid}
                                       required
                                />
                                <span className="symbol-input" style={{paddingTop: '3px'}}>
							    <i className="fa  fa-user fa-lg" aria-hidden="true"/>
						    </span>
                                {this.state.formControls.name.touched && !this.state.formControls.name.valid &&
                                <div className={"error-message-register"}> * Name is required</div>}
                            </FormGroup>

                            <FormGroup className="register-input" id='email'>
                                <Label for='emailField'> Email: </Label>
                                <Input style={input_style} name='email' id='emailField' placeholder={this.state.formControls.email.placeholder}
                                       onChange={this.handleChange}
                                       defaultValue={this.state.formControls.email.value}
                                       touched={this.state.formControls.email.touched ? 1 : 0}
                                       valid={this.state.formControls.email.valid}
                                       required
                                />
                                <span className="symbol-input" style={{paddingTop: '3px'}}>
							    <i className="fa fa-envelope fa-lg" aria-hidden="true"/>
						    </span>
                                {this.state.formControls.email.touched && !this.state.formControls.email.valid &&
                                <div className={"error-message-register"}> * Email must have a valid format</div>}
                            </FormGroup>

                            <FormGroup className="register-input" id='password'>
                                <Label for='passwordField'> Password: </Label>
                                <Input style={input_style_password} type="password" name='password' id='passwordField' placeholder={this.state.formControls.password.placeholder}
                                       onChange={this.handleChange}
                                       defaultValue={this.state.formControls.password.value}
                                       touched={this.state.formControls.password.touched ? 1 : 0}
                                       valid={this.state.formControls.password.valid}
                                       required
                                />
                                <span className="symbol-input" style={{paddingTop: '10px'}}>
							    <i className="fa  fa-lock fa-lg" aria-hidden="true"/>
						    </span>
                                {this.state.formControls.password.touched && !this.state.formControls.password.valid &&
                                <div className={"error-message-register"}> * Email must have a valid format</div>}
                            </FormGroup>

                            <FormGroup className="register-input" id='confirmpass'>
                                <Label for='confirmpassField'> Confirm Password: </Label>
                                <Input style={input_style_password} type='password' name='confirmpass' id='confirmpassField'
                                       placeholder={this.state.formControls.confirmpass.placeholder}
                                       onChange={this.handlePass}
                                       defaultValue={this.state.formControls.confirmpass.value}
                                       touched={this.state.formControls.confirmpass.touched ? 1 : 0}
                                       valid={this.state.formControls.confirmpass.valid}
                                       required
                                />
                                <span className="symbol-input" style={{paddingTop: '10px'}}>
                                <i className="fa fa-lock fa-lg" aria-hidden="true"/>
                            </span>
                                {this.state.formControls.confirmpass.touched && !this.state.formIsValid &&
                                <div className={"error-message-register"}> * Passwords must match </div>}
                            </FormGroup>
                            <br/>
                            <Row>
                                <Col sm={{size: '4', offset: 6}}>
                                    <Button type="button" className="register100-form-btn" onClick={this.handleSubmit}> Submit </Button>
                                </Col>
                            </Row>
                        </div>

                    </div>

                    </div>
                </div>

        )
    }
}

export default RegisterContainer;

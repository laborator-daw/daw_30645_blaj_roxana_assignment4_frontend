import React, {Component} from 'react'
import {BrowserRouter as Router, Route, Switch, Link} from 'react-router-dom'

import './commons/css/App.css';
import './commons/css/style.css';

import Home from "./home.js";
import Contact from "./contact.js";
import Noutati from "./noutati.js";
import Medici from "./medici.js";
import Servicii from "./servicii.js";
import Despre from "./despre.js";
import LoginContainer from "./login/login-container.js";
import RegisterContainer from "./register/register-container.js"
import ProfileContainer from "./profile/profile-container.js";
import AppointmentContainer from "./appointment/appointment-container.js";
import DoctorAppointment from "./doctor-appointment/doctor-appointment.js"
import DoctorStatistics from "./doctor-appointment/doctor-statistics.js";
import PrivateRouteDoctor from "./privateRoutes/PrivateRouteDoctor";
import PrivateRoutePatient from "./privateRoutes/PrivateRoutePatient";

import Logo from "./commons/images/fav3.png";

import Ro from "./commons/images/ro.png";
import Eng from "./commons/images/eng.png";

export default class App extends Component{

  constructor(props) {
    super(props);
    this.state = {lang: 'ro' };
    localStorage.setItem("Language", this.state.lang);
   // this.handleLanguage = this.handleLanguage.bind(this);
  }

  handleLanguage = (language) => {
    //console.log(language);
    localStorage.setItem("Language", language);
    this.setState({lang: language}    );
  };

  handleLogout() {
    localStorage.clear();
    window.location.href="/login";
  }


  render(){
    let lang;
    lang = localStorage.getItem('Language');
    return (


        <div className="App">
          <header>

            <Router>
              <div className="container">
                <div className="menu">

                  <ul>
                    <li className="menu-item"><Link to="home">  {this.state.lang === 'ro' ? 'Acasa' : 'Home' } </Link></li>
                    <li className="menu-item"><Link to="noutati">  {this.state.lang === 'ro' ? 'Noutati' : 'News' }</Link></li>
                    <li className="menu-item"><Link to="medici">  {this.state.lang === 'ro' ? 'Medici' : 'Doctors' }</Link></li>
                    <li className="menu-item"><Link to="servicii">  {this.state.lang === 'ro' ? 'Servicii&Tarife' : 'Services' }</Link></li>
                    <li className="menu-item"><Link to="despre">  {this.state.lang === 'ro' ? 'Despre noi' : 'About us' }</Link></li>
                    <li className="menu-item"><Link to="contact">  {this.state.lang === 'ro' ? 'Contact' : 'Contact' }</Link></li>

                    <li className="menu-item" id="langEn">
                      <img src={Eng}  alt="Logo Eng" style={{height: '65%'}} onClick={() => this.handleLanguage('eng')}  />

                    </li>

                    <li className="menu-item" id="langRo">
                      <img src={Ro}  alt="Logo Ro" style={{height: '65%'}} onClick={() => this.handleLanguage('ro')} />
                    </li>

                    <li className="menu-item" style={{float: 'right', marginRight: '20px', display: localStorage.getItem("id") !== null ? 'none':'block'}}><Link to="login">  {this.state.lang === 'ro' ? 'Logare' : 'Login' }</Link></li>
                    <li className="menu-item" style={{float: 'right', marginRight: '20px', display: localStorage.getItem("id") === null ? 'none':'block'}} onClick={this.handleLogout}> {this.state.lang === 'ro' ? 'Delogare' : 'Logout' }</li>

                    <li className="menu-item" style={{float: 'right', marginRight: '20px', display: (localStorage.getItem("id") !== null && localStorage.getItem("role") === 'patient') ? 'block':'none'}}><Link to="profile">  {this.state.lang === 'ro' ? 'Profil' : 'Profile' }</Link></li>
                    <li className="menu-item" style={{float: 'right', marginRight: '20px', display: (localStorage.getItem("id") !== null && localStorage.getItem("role") === 'patient') ? 'block':'none'}}><Link to="appoinment">  {this.state.lang === 'ro' ? 'Programare' : 'Appointment' }</Link></li>

                    <li className="menu-item" style={{float: 'right', marginRight: '20px', display: (localStorage.getItem("id") !== null && localStorage.getItem("role") === 'doctor') ? 'block':'none'}}><Link to="doctorAppoinment">  {this.state.lang === 'ro' ? 'Programari' : 'Appointments' }</Link></li>
                    <li className="menu-item" style={{float: 'right', marginRight: '20px', display: (localStorage.getItem("id") !== null && localStorage.getItem("role") === 'doctor') ? 'block':'none'}}><Link to="doctorStatistics">  {this.state.lang === 'ro' ? 'Statistica' : 'Statistics' }</Link></li>

                  </ul>

                </div>
              </div>


              <Switch>
                <Route exact path="/">
                  <Home languageFromMenu = {this.state.lang}/>
                </Route>
                <Route exact path="/home">
                  <Home languageFromMenu = {this.state.lang}/>
                </Route>
                <Route exact path="/noutati">
                  <Noutati languageFromMenu = {this.state.lang}/>
                </Route>
                <Route exact path="/medici">
                  <Medici languageFromMenu = {this.state.lang}/>
                </Route>
                <Route exact path="/servicii">
                  <Servicii languageFromMenu = {this.state.lang}/>
                </Route>
                <Route exact path="/despre">
                  <Despre languageFromMenu = {this.state.lang}/>
                </Route>
                <Route exact path="/contact">
                  <Contact languageFromMenu = {this.state.lang}/>
                </Route>
                <Route exact path="/login">
                  <LoginContainer languageFromMenu = {this.state.lang}/>
                </Route>
                <Route exact path="/register">
                  <RegisterContainer languageFromMenu = {this.state.lang}/>
                </Route>
                <PrivateRoutePatient exact path="/profile">
                  <ProfileContainer languageFromMenu = {this.state.lang}/>
                </PrivateRoutePatient>
                <PrivateRoutePatient exact path="/appoinment">
                  <AppointmentContainer languageFromMenu = {this.state.lang}/>
                </PrivateRoutePatient>
                <PrivateRouteDoctor exact path="/doctorAppoinment">
                  <DoctorAppointment languageFromMenu = {this.state.lang}/>
                </PrivateRouteDoctor>
                <PrivateRouteDoctor exact path="/doctorStatistics">
                  <DoctorStatistics languageFromMenu = {this.state.lang}/>
                </PrivateRouteDoctor>

              </Switch>
            </Router>
          </header>
        </div>
    );
  }
};

import './commons/css/despre.css';
import './commons/css/style.css';

import Calendar from "./commons/images/calendar1.png"
import Mail from "./commons/images/mail.png"
import Phone from "./commons/images/phone1.png"
import Eng from "./commons/images/eng.png";
import Ro from "./commons/images/ro.png";
import React, {Component} from "react";

export default class Despre extends React.Component {

    constructor(props) {
        super(props);
    }


    render() {
        return (
            <div className="container">
                <div className="containerDetails">
                    <h1>
                        {this.props.languageFromMenu === 'ro' ? 'Stomatologie moderna si echipamente de ultima generatie' :
                            'Modern dentistry and last generation equipment' }
                    </h1>
                    <p>
                        {this.props.languageFromMenu === 'ro' ? '  Cu o experienta de peste 10 ani, echipa noastra de medici stomatologi specialisti va garanteaza\n' +
                            '                        calitatea si profesionalismul tratamentelor de orice natura prin folosirea de echipamente\n' +
                            '                        moderne,\n' +
                            '                        de ultima generatie, tratamente stomatologice care contribuie la mentinerea sanatoasa si\n' +
                            '                        frumoasa a\n' +
                            '                        dintilor.' :
                            'With over 10 years of experience, our team of specialist dentists guarantees the quality and professionalism of ' +
                            'treatments of any kind by using modern, state-of-the-art equipment, dental treatments that contribute to maintaining' +
                            ' healthy and beautiful teeth.' }

                    </p>
                    <p>
                        {this.props.languageFromMenu === 'ro' ? ' Echipa noastra de medici stomatologi specialisti va sta la dispozitie intr-un mediu calm si relaxant' :
                            'Our team of specialist dentists is at your disposal in a calm and relaxing environment.' }

                    </p>

                </div>

                <div className="containerMovie">

                    <table>
                        <tbody>
                        <tr>
                            <td>
                                <iframe width="620" height="400" src="https://www.youtube.com/embed/g8kFPMKIXdY">
                                </iframe>
                            </td>
                            <td style={{width: '350px'}}>
                                <form style={{display: 'inline'}} action="contact" method="get">
                                    <button className="buttonP">
                                        {this.props.languageFromMenu === 'ro' ? 'Programeaza-te online' : 'Online appointment' }
                                    </button>
                                </form>
                                <div className="dataContainer">
                                    <img height="55" width="70" src={Calendar} alt="calendarLogo"/>
                                    <p>
                                        {this.props.languageFromMenu === 'ro' ? 'Luni-Vineri: 9:00-20:00' : 'Monday-Friday:   9:00-20:00' }
                                    </p>

                                    <hr></hr>
                                    <img height="55" width="70" src={Mail} alt="mailLogo"/>
                                    <p>office@dentaweb.ro</p>
                                    <hr></hr>
                                    <img height="55" width="70" src={Phone} alt="phoneLogo"/>
                                    <p> +40 740 825 000 </p>
                                </div>

                            </td>
                        </tr>
                        </tbody>
                    </table>

                </div>
            </div>

        );
    }
}
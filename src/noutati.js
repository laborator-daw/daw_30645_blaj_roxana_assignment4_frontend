import React, {Component} from "react";
import { useState } from "react";
import {Container} from  "react-bootstrap";

import "./commons/css/noutati.css";
import './commons/css/style.css';

import XMLData from './commons/xml/noutati.xml';
import App from './App';
import Eng from "./commons/images/eng.png";
import Ro from "./commons/images/ro.png";

export default  class Noutati extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            lists: [],
            listItems:[],
            xml: ''
        }
    }


    fetchXML(){
        const rawFile = new XMLHttpRequest();
        rawFile.onreadystatechange = () => {
            if (rawFile.readyState === 4 && (rawFile.status === 200 || rawFile.status === 0)) {
                this.parseXML(rawFile);
            }
        };
        rawFile.open('GET', XMLData , false);
        rawFile.send();
    }

    parseXML(rawFile){
        const parser = new DOMParser();
        const xml = parser.parseFromString(rawFile.response, 'text/xml');
        this.state.xml = xml;
        this.getDataFromXML(xml);
        //let lenght = xml.getElementsByTagName(lang.toString())[0].children.length;
    }

    getDataFromXML(xml){
        let lang = this.props.languageFromMenu;
        console.log(lang);
        let lenght = xml.getElementsByTagName(lang.toString())[0].children.length;
        this.state.lists = [];
        for (let i = 0; i < lenght; i++) {
            let title = xml.getElementsByTagName(lang.toString())[0].children[i].children[0].textContent;
            let desc = xml.getElementsByTagName(lang.toString())[0].children[i].children[1].textContent;
            this.state.lists.push({
                title: title,
                description: desc
            });
        }
    };

    setGalleryData(){
        let list = this.state.lists.map((listaDate,index) =>
            <div className="gallery" >
                <div className="desc" key={listaDate.description.index}>
                    {listaDate.description}
                </div>
                <hr/>
                <div className="desc" key={listaDate.title.index} style={{fontStyle: 'italic'}}>
                    {listaDate.title}
                </div>
            </div>
        );

        this.setState({
            listItems: list
        });
    }


    render() {
        this.fetchXML();

        return (
            <div className="container">
                <div className="container">

                    <div className="container set-margin-top-40px">

                        {
                            //this.state.listItems,
                            this.state.lists.map((listaDate,index) =>
                                <div className="gallery" >
                                    <div className="desc" key={listaDate.description.index}>
                                        {listaDate.description}
                                    </div>
                                    <hr/>
                                    <div className="desc" key={listaDate.title.index} style={{fontStyle: 'italic'}}>
                                        {listaDate.title}
                                    </div>
                                </div>
                            )
                        }


                    </div>
                </div>

            </div>
        );
    };

}
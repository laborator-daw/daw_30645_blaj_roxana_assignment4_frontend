import './commons/css/servicii.css';
import './commons/css/style.css';
import React, {Component} from 'react';


export default class Servicii extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="container">
                <h2 style={{marginleft: '18%'}}>
                    {this.props.languageFromMenu === 'ro' ? 'Tarife&Servicii Cabinet Stomatologic DetaWEB' : 'Tariffs&Services DentaWEB Dental Office' }

                </h2>

                <table id="tabelTarife">
                    <tbody>
                    <tr>
                        <th>
                            {this.props.languageFromMenu === 'ro' ? 'Tip Investigatie' : 'Investigation type' }
                        </th>
                        <th>
                            {this.props.languageFromMenu === 'ro' ? 'Pret' : 'Price' }
                        </th>
                    </tr>
                    <tr>
                        <td>
                            {this.props.languageFromMenu === 'ro' ? 'Consultatie' : 'Examination' }
                        </td>
                        <td>70 lei</td>
                    </tr>
                    <tr>
                        <td>
                            {this.props.languageFromMenu === 'ro' ? ' Pachet consultatie cu dus bucal inclus' : 'Consultation package with mouth shower included' }
                        </td>
                        <td>120 lei</td>
                    </tr>
                    <tr>
                        <th>
                            {this.props.languageFromMenu === 'ro' ? 'PROFILAXIE' : 'PROPHYLAXIS' }
                        </th>
                        <th></th>
                    </tr>
                    <tr>
                        <td>
                            {this.props.languageFromMenu === 'ro' ? 'Detartraj ultrasunete' : 'Ultrasonic descaling' }
                        </td>
                        <td>150 lei</td>
                    </tr>
                    <tr>
                        <td>
                            {this.props.languageFromMenu === 'ro' ? 'Detartraj pulbere de bicarbonat' : 'Descaling baking soda powder' }
                        </td>
                        <td>150 lei</td>
                    </tr>
                    <tr>
                        <td>
                            {this.props.languageFromMenu === 'ro' ? ' Periaj profesional / arcada' : 'Professional brushes / arcade' }
                        </td>
                        <td>50 lei</td>
                    </tr>
                    <tr>
                        <td>
                            {this.props.languageFromMenu === 'ro' ? 'Fluorizare' : 'Fluoridation' }
                        </td>
                        <td>50 lei</td>
                    </tr>
                    <tr>
                        <th>
                            {this.props.languageFromMenu === 'ro' ? 'RESTAURARI CORONARE' : 'CORONARY RESTORATIONS' }
                        </th>
                        <th></th>
                    </tr>
                    <tr>
                        <td>
                            {this.props.languageFromMenu === 'ro' ? 'Pansament pulpomixine + fermin' : 'Pulpomixin + fermin application' }
                        </td>
                        <td>100 lei</td>
                    </tr>
                    <tr>
                        <td>
                            {this.props.languageFromMenu === 'ro' ? 'Obturatii 1 fata' : '1 face fillings' }</td>
                        <td>150 lei</td>
                    </tr>
                    <tr>
                        <td>
                            {this.props.languageFromMenu === 'ro' ? 'Obturatii 2 fate' : '2 faces fillings' }
                        </td>
                        <td>200-250 lei</td>
                    </tr>
                    <tr>
                        <td>
                            {this.props.languageFromMenu === 'ro' ? 'Obturatii grup frontal' : 'Front group fillings' }
                        </td>
                        <td>200 lei</td>
                    </tr>
                    <tr>
                        <td>
                            {this.props.languageFromMenu === 'ro' ? 'Sigilare santuri si fosete' : 'Sealing ditches and ditches' }
                        </td>
                        <td>80 lei</td>
                    </tr>
                    <tr>
                        <td>
                            {this.props.languageFromMenu === 'ro' ? 'Obturatii colorate copii' : 'Colorful children\'s fillings' }
                        </td>
                        <td>100 lei</td>
                    </tr>
                    <tr>
                        <td>
                            {this.props.languageFromMenu === 'ro' ? '  Baza glass-ionomer foto' : 'Glass-ionomer base photo' }
                        </td>
                        <td>40 lei</td>
                    </tr>
                    <tr>
                        <th>
                            {this.props.languageFromMenu === 'ro' ? 'ESTETICA DENTARA' : 'DENTAL AESTHETICS' }
                        </th>
                        <th></th>
                    </tr>
                    <tr>
                        <td>
                            {this.props.languageFromMenu === 'ro' ? 'Albire dentara – Opalescence Quick 35% – in cabinet' : 'Tooth whitening - Opalescence Quick 35% - in the office' }
                        </td>
                        <td>1250 lei</td>
                    </tr>
                    <tr>
                        <td>
                            {this.props.languageFromMenu === 'ro' ? 'Albire dentara – Opalescence P.F. 15% – in gutiere' : 'Tooth whitening - Opalescence P.F. 15% - in gutters' }
                        </td>
                        <td>600 lei</td>
                    </tr>
                    <tr>
                        <td>
                            {this.props.languageFromMenu === 'ro' ? 'Pret' : 'Price' }
                            Albire Endo – Opalescence Endo</td>
                        <td>100 lei</td>
                    </tr>
                    <tr>
                        <td>
                            {this.props.languageFromMenu === 'ro' ? 'Albire Ritter Bleach 37% – in cabinet' : 'Albire Ritter Bleach 37% - in the office' }
                        </td>
                        <td>2440 lei</td>
                    </tr>
                    <tr>
                        <td>
                            {this.props.languageFromMenu === 'ro' ? 'Gutiere de albire' : 'Bleaching splints' }
                        </td>
                        <td>150 lei/pereche</td>
                    </tr>
                    <tr>
                        <td>
                            {this.props.languageFromMenu === 'ro' ? 'Perlute dentare' : 'Dental pearls' }
                        </td>
                        <td>300 lei</td>
                    </tr>
                    <tr>
                        <th>
                            {this.props.languageFromMenu === 'ro' ? 'PROTETICA' : 'PROSTHETICS' }
                        </th>
                        <th></th>
                    </tr>
                    <tr>
                        <td>
                            {this.props.languageFromMenu === 'ro' ? ' Cimentat adezor punte/dinte' : 'Cemented bridge / tooth adhesive' }
                        </td>
                        <td>70 lei</td>
                    </tr>
                    <tr>
                        <td>
                            {this.props.languageFromMenu === 'ro' ? 'Indepartat punte/dinte' : 'Removed bridge / tooth' }
                        </td>
                        <td>30 lei</td>
                    </tr>
                    <tr>
                        <td>
                            {this.props.languageFromMenu === 'ro' ? 'Coroana metalica Cr-Ni' : 'Cr-Ni metal crown' }
                        </td>
                        <td>250 lei</td>
                    </tr>
                    <tr>
                        <td>
                            {this.props.languageFromMenu === 'ro' ? 'Coroana provizorie acrilica' : 'Temporary acrylic crown' }
                        </td>
                        <td>150 lei</td>
                    </tr>
                    <tr>
                        <td>
                            {this.props.languageFromMenu === 'ro' ? ' Coroana semifizionomica metalo acrilica' : 'Acrylic metal semi-physiognomic crown' }
                           </td>
                        <td>350 lei</td>
                    </tr>
                    <tr>
                        <td>
                            {this.props.languageFromMenu === 'ro' ? '  Coroana integral ceramica CAD-CAM' : 'CAD-CAM all-ceramic crown' }
                          </td>
                        <td>1220 lei</td>
                    </tr>
                    <tr>
                        <td>
                            {this.props.languageFromMenu === 'ro' ? ' Fateta integral ceramica CAD-CAM' : 'Integral ceramic facet CAD-CAM' }
                        </td>
                        <td>1220 lei</td>
                    </tr>
                    <tr>
                        <td>
                            {this.props.languageFromMenu === 'ro' ? 'Coroana fizionomica zirconiu – ceramica' : 'Zirconium-ceramic physiognomic crown' }
                        </td>
                        <td>1220 lei</td>
                    </tr>
                    <tr>
                        <td>
                            {this.props.languageFromMenu === 'ro' ? ' Coroana integral ceramica' : 'All-ceramic crown' }
                           </td>
                        <td>1220 lei</td>
                    </tr>
                    <tr>
                        <td>
                            {this.props.languageFromMenu === 'ro' ? 'Proteza totala / partiala acrilica' : 'Total / partial acrylic prosthesis' }
                        </td>
                        <td>900 lei</td>
                    </tr>
                    <tr>
                        <td>
                            {this.props.languageFromMenu === 'ro' ? 'Proteza totala / partiala elastica/injectata' : 'Total / partial elastic / injected prosthesis' }
                            </td>
                        <td>1100/1500 lei</td>
                    </tr>
                    </tbody>
                </table>

            </div>


        );
    }
}
import React, { useEffect, useState } from "react";
import DataPicker, { setDefaultLocale } from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

export const Calendar = ({ selected, onSelectedChange }) => {
    const [date, setDate] = useState(selected);

    useEffect(() => {
        setDate(selected);
    }, [selected]);

    return (
        <DataPicker selected={date} onChange={(date) => onSelectedChange(date)} />
    );
};

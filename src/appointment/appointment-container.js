import React, {useState} from 'react';
import "../commons/css/appointment.css"
import "../commons/css/profile.css"

import { Calendar } from "./calendar-component";
import {FormComponent} from "./form-component";
import {ListComponent} from "./list-component";
import {FormGroup, Input, Label} from "reactstrap";
import { Button } from "react-bootstrap";


const input_style = {
    lineHeight: 1.5,
    color: '#666666',
    display: 'block',
    width: '100%',
    background: '#e6e6e6',
    height: '50px',
    borderRadius: '25px',
    padding: '0 30px 0 68px',
    marginBottom: '30px',
};

export default function AppointmentContainer() {

    const [selectedDate, setDate] = useState(new Date());
    const [selectedHour, setSelectedHour] = useState([]);
    const [openForm, setOpenForm] = useState(false);
    const [items, setItems] = useState([]);
    const [doctors, setDoctors] = useState([]);
    const [medServs, setMedServs] = useState([]);

    const selectedDateChange = (date) => {
        setDate(date);
    };

    const addItems = (item) => {
        setItems((oldItem) => [...oldItem, item]);
    };

    const addSelectedHour = (hour) => {
        setSelectedHour((oldHours) => [...oldHours, hour]);
    };

    const addDoctor = (doctor) => {
        setDoctors((oldDoctors) => [...oldDoctors, doctor]);
    };

    const addMedServ = (medServ) => {
        setMedServs((oldMedServs) => [...oldMedServs, medServ]);
    };

    return (
            <div className="appointment-page">

                    <div className="container-profile100">
                        <div className="wrap-profile100">
                         <span className="profile100-form-title">
                                Make an appointment
                         </span>
                         <div className="profile-inputs">

                             <Label className='txt1'> Select appointment date: </Label>
                             <Calendar selected={selectedDate} onSelectedChange={selectedDateChange} />

                             <FormComponent
                                 selectedDate={selectedDate}
                                 addSelectedHour={addSelectedHour}
                                 //addItem={addItems}
                                 addSelectedDoctor={addDoctor}
                                 addSelectedMedOption={addMedServ}
                             />
                             <br/>
                             <Label className='txt1'> All appointments: </Label>
                             <br/><br/><br/>
                             <ListComponent />
                         </div>


                        </div>

                    </div>
            </div>
    )

}


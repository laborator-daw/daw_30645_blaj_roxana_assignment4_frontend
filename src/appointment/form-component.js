import React, {useEffect, useState} from "react";
import {Modal, ModalBody, Button, Form, Label, Col, Row, Input, FormGroup} from 'reactstrap'
import axios from "axios";

export const FormComponent = ({ selectedDate, addSelectedHour, addSelectedDoctor, addSelectedMedOption }) => {
    const [item, setItem] = useState();
    const [selectedDoctor, setSelectedDoctor] = useState("");
    const [selectedHour, setSelectedHour] = useState(8);
    const [selectedMedOption, setSelectedMedOption] = useState("");


    const handleClick = () => {

        //addItem(item);
        addSelectedHour(selectedHour);
        addSelectedDoctor(selectedDoctor);
        addSelectedMedOption(selectedMedOption);
        //console.log(selectedDate + " " + selectedHour + " " + selectedDoctor + " " + selectedMedOption );
        var appointmentDate = convertDate(selectedDate);

        let appointment = {
            appointmentDate: appointmentDate,
            appointmentHour: selectedHour,
            patient: localStorage.getItem("id"),
            doctor: selectedDoctor,
            medServApp: selectedMedOption
        }

        console.log(appointment);

        axios.post(
            "http://127.0.0.1:8000/api/appointment/",
            appointment
        ).then((response) => {
            if(response.status === 201){
                window.location.reload(false);
                alert("Appointment created succesfully: " + appointmentDate + "("+ selectedHour + ") for:" +selectedMedOption+"("+selectedDoctor+")");
            }
            else if(response.data.status === "failed")
                alert(selectedDoctor+ " is not available for" + selectedDate+"("+selectedHour+"). Please try another date or hour!");
        });


    };

    const convertDate = (selectedDate) => {
        var date = new Date(selectedDate),
            mnth = ("0" + (date.getMonth() + 1)).slice(-2),
            day = ("0" + date.getDate()).slice(-2);
        return  [date.getFullYear(), mnth, day].join("-");
    }

    const onChangeHourSelection = (e) => {
        setSelectedHour(e.target.value);
    };

    const onChangeSelection = (e) => {
        setSelectedDoctor(e.target.value);
    };

    const onChangeMedSelection = (e) => {
        setSelectedMedOption(e.target.value);
    };

    return (
        <div>
            <br />
            <Row>
                <Col>
                    <label for="date">Selected date:</label>
                </Col>
                <Col>
                    <input
                        type="text"
                        id="date"
                        disabled
                        value={selectedDate.toDateString()}
                    ></input>
                </Col>
            </Row>

            <Row>
                <Col>
                    <label for="hour">Select hour:</label>
                </Col>
                <Col>
                    <Input
                        type="select"
                        id="hour"
                        value = {selectedHour}
                        onChange={
                            onChangeHourSelection
                        }
                        size={"4"}
                    >
                        <option value="" hidden>Choose hour...</option>
                        <option value="8">08:00</option>
                        <option value="9">09:00</option>
                        <option value="10">10:00</option>
                        <option value="11">11:00</option>
                        <option value="12">12:00</option>
                        <option value="14">14:00</option>
                        <option value="15">15:00</option>
                        <option value="16">16:00</option>


                    </Input>
                </Col>
            </Row>

            <Row>
                <Col>
                    <label for="doctor">Select doctor:</label>
                </Col>
                <Col>
                    <Input
                        type="select"
                        id="doctor"
                        value = {selectedDoctor}
                        onChange={
                            onChangeSelection
                        }
                        size={"4"}
                    >
                        <option value="" hidden>Choose doctor...</option>
                        <option value="2">Dr. John Doe</option>
                        <option value="3">Dr. Kate Clare </option>
                        <option value="4">Dr. Delia Lorinda</option>
                        <option value="5">Dr. Bristol Kirsten</option>

                    </Input>
                </Col>
            </Row>

            <Row>
                <Col>
                    <label for="medServ">Select medical service:</label>
                </Col>
                <Col>
                    <Input
                        type="select"
                        id={"medServ"}
                        value = {selectedMedOption}
                        onChange={
                            onChangeMedSelection
                        }
                        size={"5"}
                    >
                        <option value="0" hidden>Choose medical services...</option>
                        <option value="Consultatie">Consultatie</option>
                        <option value="Detartraj">Detartraj</option>
                        <option value="Periaj profesional">Periaj profesional</option>
                        <option value="Sigilare santuri">Sigilare santuri</option>
                        <option value="Obturatii ">Obturatii</option>
                        <option value="Albire">Albire  </option>
                        <option value="Gutiere de albire">Gutiere de albire </option>
                        <option value="Indepartat punte">Indepartat punte </option>
                        <option value="Coroana metalica Cr-Ni">Coroana metalica Cr-Ni </option>
                        <option value="Coroana provizorie acrilica">Coroana provizorie acrilica</option>
                        <option value="Coroana integral ceramica">Coroana integral ceramica</option>
                        <option value="Proteza">Proteza  </option>
                        <option value="Fatete">Fatete </option>

                    </Input>
                </Col>
            </Row>
            <br />

            <Row>
                <Col></Col>
                <Col>
                    <Button className='appointment100-form-btn' onClick={handleClick}>
                        ADD
                    </Button>
                </Col>

                <Col></Col>
            </Row>
     </div>
    );
};
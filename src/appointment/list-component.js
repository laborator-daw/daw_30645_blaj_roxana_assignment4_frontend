import React, { useEffect, useState } from "react";
import { ListGroup, ListGroupItem } from "react-bootstrap";
import axios from "axios";

export const ListComponent = () => {

    const [data, setData] = useState([]);

    useEffect(() => {
        axios.get("http://127.0.0.1:8000/api/appointment").then((res) => {
            setData(res.data.data);
        });
    }, []);
    let id =  localStorage.getItem("id");

    const listElem = (
        <ListGroup>
            {data
                .map((el) => {
                     if(el.patient == id)
                        return( <div>
                            <p>Appointment date: {el.appointmentDate}({el.appointmentHour}:00), doctor: {el.doctor} for {el.medServApp}</p>
                            <br /><br />
                            <hr />
                            <br />
                        </div>)
                    })
            }
        </ListGroup>
    );

    return (
        <div>{listElem}</div>
    );
};
import React, {Component} from 'react';
import Plotly from 'plotly.js';
import createPlotlyComponent from 'react-plotly.js/factory';
import axios from "axios";
const Plot = createPlotlyComponent(Plotly);

class DoctorStatistics extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            dateInMonth: [],
            valueForDate:[]
        };
        this.getStatistics();
    }

    getStatistics(){

        let dates=[];

        let dt = new Date();

        let month = dt.getMonth() + 1;
        let year = dt.getFullYear();

        let daysInMonth = new Date(year, month, 0).getDate();

        for(let i=1; i<=daysInMonth; i++){
            let date, monthZ;

            if(month>=1 && month<=9)
                monthZ ="0"+month
            else monthZ = month;

            if(i>=1 && i<=9)
                date = year + "-" + monthZ + "-0" + i;
            else
                date = year + "-" + monthZ + "-" + i;

            let appointmentObj = {
                dateKey: date,
                dateValue: 0
            }
            dates.push(appointmentObj);
        }


        axios.get(
            "http://127.0.0.1:8000/api/getAppointmentForDoctor/" + localStorage.getItem('id'),
        ).then((response) => {
            if (response.status === 200) {

                let lenght = response.data.data.length;
                let data = response.data.data;

                for(let i=0; i<lenght;i++){
                    let date = data[i].appointmentDate;
                    let dateSplit = date.split("-");

                    let day = dateSplit[2];
                    let regex = /0[1-9]/;
                    let dayD;
                    if(day.match(regex)){
                        dayD = day[1] -1 ;
                    }
                    else dayD=day - 1;
                    dates[dayD].dateValue++;
                }

                let vv1 = [], vv2=[];
                for(let i=0; i<daysInMonth; i++) {
                    vv1.push(dates[i].dateKey);
                    vv2.push(dates[i].dateValue);
                }

                this.setState({
                    dateInMonth: vv1,
                    valueForDate: vv2
                });
            }
        });
    }

    render(){
        return(
            <Plot
                data={
                    [{
                        x: this.state.dateInMonth,
                        y: this.state.valueForDate,
                        type: 'scatter',
                        marker: {color: '#142952'},
                    }]
                }
                layout={{width: 1000, height: 500, title: 'Monthly statistics'}}
            />
        );
    }

}

export default DoctorStatistics;
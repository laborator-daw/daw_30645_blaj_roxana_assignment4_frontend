import React from 'react';
import {Inject, ScheduleComponent, Day, Week, WorkWeek, Month, Agenda, EventSettingsModel, ViewsDirective, ViewDirective} from '@syncfusion/ej2-react-schedule';
import axios from "axios";

class DoctorAppointment extends React.Component {

    constructor() {
        super(...arguments);
        //this.getAppointments = this.getAppointments.bind(this);
        this.state ={
            appointmentData: []
        }
        this.getAppointments();
    }

    getAppointments(){

        let doctorId = localStorage.getItem('id');
        console.log(doctorId);
        axios.get(
            "http://127.0.0.1:8000/api/getAppointmentForDoctor/" + doctorId,
        ).then((response) => {
                console.log(response.data);
                console.log(response.status);
                if (response.status === 200) {
                    //let year = response.data.length
                    let lenght = response.data.data.length;
                    let data = response.data.data;
                    console.log(lenght);
                    let i;
                    let appointmentArray=[];
                    for(i=0; i<lenght;i++){
                        let date = data[i].appointmentDate;
                        let dateSplit = date.split("-");

                        let year = dateSplit[0];
                        let month = dateSplit[1];
                        let day = dateSplit[2];

                        let time = data[i].appointmentHour;
                        let serv = data[i].medServApp;

                        let appointment = {Subject: serv,
                            EndTime: new Date(year, month-1, day, time+1, 0),
                            StartTime: new Date(year, month-1, day, time, 0),
                            IsAllDay: false
                        }
                        appointmentArray.push(appointment);
                    }
                    console.log(appointmentArray);
                    this.setState({
                        appointmentData: appointmentArray
                    });
                }
            });
    };

    render(){
     return(
         <ScheduleComponent   currentView={'Month'} startHour='8:00' endHour='18:00'
                            eventSettings={{ dataSource: this.state.appointmentData,
                                fields: {
                                    subject: { name: 'Subject' },
                                    isAllDay: { name: 'IsAllDay' },
                                    startTime: { name: 'StartTime' },
                                    endTime: { name: 'EndTime' }
                                }
                            }}>
             <ViewsDirective>
                 <ViewDirective option='Week'/>
                 <ViewDirective option='Month'/>
             </ViewsDirective>
             <Inject services={[ Week, Month]}/>


         </ScheduleComponent>
     );
    }
}
export default DoctorAppointment;

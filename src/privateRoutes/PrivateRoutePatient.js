import React from 'react';
import {BrowserRouter as Router, Route, Switch, Link, Redirect} from 'react-router-dom'

function PrivateRoutePatient({children, ...rest}) {
    return (
        <Route {...rest} render={() => {
            return (localStorage.getItem("id")!==null && localStorage.getItem("role")==='patient') ? children :  <Redirect to="/home"/>
        }}>

        </Route>
    )
}
export default PrivateRoutePatient;
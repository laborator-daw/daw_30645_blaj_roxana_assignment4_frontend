import React from 'react';
import {BrowserRouter as Router, Route, Switch, Link, Redirect} from 'react-router-dom'

function PrivateRouteDoctor({children, ...rest}) {
    return (
        <Route {...rest} render={() => {
            return (localStorage.getItem("id")!==null && localStorage.getItem("role")==='doctor') ? children : <Redirect to="/home"/>
        }}>

        </Route>
    )
}
export default PrivateRouteDoctor;
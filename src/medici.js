import './commons/css/medici.css';
import './commons/css/style.css';

import AvatarMiss from "./commons/images/dr1.jpg"
import AvatarMs from "./commons/images/dr2.jpg"
import React, {Component} from "react";

export default class Medici extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (

            <div className="container" style={{marginLeft: '-5%'}}>

                <table>
                    <tbody>
                    <tr>
                        <td>
                            <div className="container">
                                <div className="container-numedr">
                                    <h3>Dr. John Doe</h3>
                                </div>
                                <div className="container">
                                    <p>
                                        {this.props.languageFromMenu === 'ro' ? 'Medic stomatolog, manager general' : 'Dentist, general manager'}
                                    </p>
                                    <hr></hr>
                                    <img src={AvatarMs} alt="Avatar" className="img-doctor"
                                         style={{width: '80px'}}/>
                                    <span>
                                                  {this.props.languageFromMenu === 'ro' ? ' Competente:  Implantologie & Tehnologia Cad-Cam, stomatologie 3D' : 'Competences: Implantology & Cad-Cam Technology, 3D dentistry'}
                                            </span><br></br>

                                </div>
                                <form style={{display: 'inline'}} action="contact" method="get">
                                    <button className="buttonM">
                                        {this.props.languageFromMenu === 'ro' ? '+ Programare' : '+ Appointment'}
                                    </button>
                                </form>
                            </div>
                        </td>

                        <td>

                            <div className="container">
                                <div className="container-numedr">
                                    <h3>Dr. Kate Clare </h3>
                                </div>
                                <div className="container">
                                    <p>
                                        {this.props.languageFromMenu === 'ro' ? ' Medic stomatolog' : 'Dentist'}
                                    </p>
                                    <hr></hr>
                                    <img src={AvatarMiss} alt="Avatar" className="img-doctor"
                                         style={{width: '80px'}}/>
                                    <span>
                                                  {this.props.languageFromMenu === 'ro' ? ' Competente:  Implantologie & Tehnologia Cad-Cam, stomatologie 3D' : 'Competences: Implantology & Cad-Cam Technology, 3D dentistry'}
                                             </span><br></br>

                                </div>
                                <form style={{display: 'inline'}} action="contact" method="get">
                                    <button className="buttonM">
                                        {this.props.languageFromMenu === 'ro' ? '+ Programare' : '+ Appointment'}
                                    </button>
                                </form>
                            </div>

                        </td>
                    </tr>
                    <tr>

                        <td>
                            <div className="container">
                                <div className="container-numedr">
                                    <h3>Dr. Delia Lorinda </h3>
                                </div>
                                <div className="container set-margin-left-20px">
                                    <p>
                                        {this.props.languageFromMenu === 'ro' ? ' Medic stomatolog' : 'Dentist'}
                                    </p>
                                    <hr></hr>
                                    <img src={AvatarMiss} alt="Avatar" className="img-doctor"
                                         style={{width: '80px'}}/>
                                    <span>
                                                  {this.props.languageFromMenu === 'ro' ? ' Competente:  Implantologie & Tehnologia Cad-Cam, stomatologie 3D' : 'Competences: Implantology & Cad-Cam Technology, 3D dentistry'}
                                        </span><br></br>

                                </div>
                                <form style={{display: 'inline'}} action="contact" method="get">
                                    <button className="buttonM">
                                        {this.props.languageFromMenu === 'ro' ? '+ Programare' : '+ Appointment'}
                                    </button>
                                </form>
                            </div>
                        </td>

                        <td>
                            <div className="container">
                                <div className="container-numedr">
                                    <h3>Dr. Bristol Kirsten </h3>
                                </div>
                                <div className="container set-margin-left-20px">
                                    <p>
                                        {this.props.languageFromMenu === 'ro' ? ' Medic stomatolog' : 'Dentist'}
                                    </p>
                                    <hr></hr>
                                    <img src={AvatarMs} alt="Avatar" className="img-doctor"
                                         style={{width: '80px'}}/>
                                    <span>
                                                  {this.props.languageFromMenu === 'ro' ? ' Competente:  Implantologie & Tehnologia Cad-Cam, stomatologie 3D' : 'Competences: Implantology & Cad-Cam Technology, 3D dentistry'}
                                            </span><br></br>

                                </div>
                                <form style={{display: 'inline'}} action="contact" method="get">
                                    <button className="buttonM">
                                        {this.props.languageFromMenu === 'ro' ? '+ Programare' : '+ Appointment'}
                                    </button>
                                </form>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>

        );
    }
}
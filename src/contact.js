import './commons/css/contact.css';
import './commons/css/style.css';
import React, {Component} from "react";

import Calendar from "./commons/images/calendar1.png";
import Mail from "./commons/images/mail.png";
import Phone from "./commons/images/phone1.png";

import axios from 'axios';
import Eng from "./commons/images/eng.png";
import Ro from "./commons/images/ro.png";

export default class Contact extends React.Component{

    constructor(props){
        super(props);
        this.sendEmail = this.sendEmail.bind(this);
    }


    sendEmail = (e) =>{
        e.preventDefault();
        let name = document.getElementById("name").value;
        let email = document.getElementById("email").value;
        let reason= document.getElementById("reason").value;
        let  msgs= document.getElementById("msg").value;

        let formData = new FormData(document.getElementById("sendMailForm"));
        formData.append( "name", name);
        formData.append( "email", email);
        formData.append( "reason", reason);
        formData.append( "msg", msgs);
        axios
            .post("http://127.0.0.1:5000/sendMail", formData)
            .then((res) => {
                if(res.data.sendSuccesfully===1)
                {
                    alert("Mail trimis cu succes");
                    document.getElementById("sendMailForm").reset();
                }
            });
    };

    render(){
        return(
            <div className="container">

                <h2 className="set-margin-left-170px set-margin-top-40px"> Contact </h2>

                <table>
                    <tbody>
                    <tr>
                        <td style={{width: '600px'}}>
                            <div className="container dimContactForm">
                                <p >
                                    {this.props.languageFromMenu === 'ro' ? ' Completeaza formularul de mai jos si iti vom raspunde in cel mai scurt timp posibil.' :
                                        'Fill in the form below and we will answer you as soon as possible.' }
                                </p>
                                <form onSubmit={this.sendEmail} method="post" id="sendMailForm">
                                    <label className="set-margin-left-20px set-margin-right-10px"
                                           htmlFor="name">
                                        {this.props.languageFromMenu === 'ro' ? 'Nume' : 'Name' }
                                    </label>
                                    <input type="text" id="name" name="name" placeholder="Your name.."></input>
                                        <br/>
                                    <label className="set-margin-left-20px set-margin-right-10px" htmlFor="email">Email</label>
                                    <input type="text" id="email" name="email" placeholder="Your email address.."></input>
                                            <br/>
                                            <label className="set-margin-left-20px set-margin-right-12px"
                                                   htmlFor="reason">
                                                {this.props.languageFromMenu === 'ro' ? 'Motiv' : 'Subject' }
                                            </label>
                                            <select id="reason" name="reason">
                                                <option value="appointment">
                                                    {this.props.languageFromMenu === 'ro' ? 'Programare' : 'Appointment' }
                                                </option>
                                                <option value="questions">
                                                    {this.props.languageFromMenu === 'ro' ? 'Intrebari' : 'Questions' }
                                                </option>
                                                <option value="feedback">
                                                    {this.props.languageFromMenu === 'ro' ? 'Parere' : 'Feedback' }

                                                </option>
                                            </select>
                                            <br/>
                                            <textarea id="msg"></textarea>

                                    <input type="submit" value="Submit"></input>
                                </form>
                            </div>
                        </td>

                        <td style={{width: '320px'}}>
                            <div className="container">
                                <div className="dataContainer">
                                    <img height="55" width="70" src={Calendar} alt="calendarLogo"/>
                                    <p>
                                        {this.props.languageFromMenu === 'ro' ? ' Luni-Vineri: 9:00-20:00' : 'Monday-Friday: 9:00-20:00' }
                                    </p>

                                    <hr></hr>
                                        <img height="55" width="70" src={Mail} alt="mailLogo"/>
                                        <p>office@dentaweb.ro</p>
                                        <hr></hr>
                                            <img height="55" width="70" src={Phone} alt="phonoLogo"/>
                                            <p> +40 740 825 000 </p>
                                </div>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>

            </div>

        );}
}
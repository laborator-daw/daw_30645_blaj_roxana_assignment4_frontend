import './commons/css/style.css';
import './commons/css/home.css';

import React, {Component} from 'react';

import ImagePres from "./commons/images/stoma.jpg";
import ImageLogo1 from "./commons/images/m3.png";
import ImageLogo2 from "./commons/images/m1.png";
import ImageLogo3 from "./commons/images/m2.png";


export default class Home extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="container">
                <div className="container">
                    <img src={ImagePres} alt="PozaPrez" className="width100"/>
                    <div className="overlay">
                          <div className="text">
                              {this.props.languageFromMenu === 'ro' ? 'Cabinet stomatologic DentaWEB' : 'DentaWEB dentistry' }
                          </div>
                    </div>
                </div>
                <p style={{fontSize: '30px', marginLeft: '-5%', fontStyle: 'italic', textAlign: 'center'}}>
                    {this.props.languageFromMenu === 'ro' ? ' De ce sa ne alegi pe noi?' : 'Why choose us?' }
                    </p>
                <div className="whyusContainer">

                    <table>
                        <tbody>
                        <tr>
                            <td>
                                <img src={ImageLogo1} alt="Logo1"/>
                                <p style={{fontSize: '25px', textAlign: 'center'}}>
                                    {this.props.languageFromMenu === 'ro'? 'STERILIZARE CU APARATE DE ULTIMA    GENERATIE' : 'STERILIZATION WITH LATEST GENERATION APPLIANCES' }
                                     </p>
                            </td>
                            <td>
                                <img src={ImageLogo2} alt="Logo2"/>
                                <p style={{fontSize: '25px', textAlign: 'center'}}>
                                    {this.props.languageFromMenu === 'ro' ? '10 ANI DE EXPERIENȚĂ!' : '10 YEARS OF EXPERIENCE!' }

                                </p>
                            </td>
                            <td>
                                <img src={ImageLogo3} alt="Logo3"/>
                                <p style={{fontSize: '25px', textAlign: 'center'}}>
                                    {this.props.languageFromMenu === 'ro' ? 'TEHNOLOGIE DE ULTIMA GENERAȚIE' : 'LATEST GENERATION TECHNOLOGY' }
                                </p>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        );
    }
}
import React from 'react';
import "../commons/css/login.css"
import Img from "../commons/images/img-01.png"
import '../commons/fonts/font-awesome-4.7.0/css/font-awesome.min.css'
import axios from "axios";

class LoginContainer extends React.Component {

    constructor(props) {
        super(props);

        this.state = {

            errorStatus: 0,
            error: null,
            err: false,
            errorMessage: '',

            formIsValid: false,
            showErrMsg: false,
            formControls: {
                email: {
                    value: '',
                    placeholder: 'Email...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        emailValidator: true,
                        isRequired: true
                    }
                },
                password: {
                    value: '',
                    placeholder: 'Password...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        isRequired: true
                    }
                },

            }
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;
        const updatedControls = this.state.formControls;
        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedControls[name] = updatedFormElement;

        this.setState({
            formControls: updatedControls,
        });
    };

    handleSubmit() {

        let credentials = {
            email: this.state.formControls.email.value,
            password: this.state.formControls.password.value
        };

        axios.post(
            "http://127.0.0.1:8000/api/login",
            credentials
        )
            .then((response) => {
                console.log(response);
                if (response.data.status === "ok") {
                   // alert(response.data.data.id);
                    localStorage.setItem("id", response.data.data.id);
                    localStorage.setItem("role", response.data.data.role);
                    if(response.data.data.role === 'patient')
                        window.location.href="/profile";
                    if(response.data.data.role === 'doctor')
                        window.location.href="/doctorAppoinment";
                }
                if (response.data.status === "failed") {
                    console.log(response.data.message);
                    alert("Incorrect Credentials! ");

                }
            });


    }

    render() {
        return (
            <div className="limiter">
                <div className="container-login100">
                    <div className="wrap-login100">
                        <div className="login100-pic" >
                            <img src={Img} alt="IMG"/>
                        </div>

                        <form className="login100-form validate-form" id={"loginForm"}>
                            <span className="login100-form-title">
                                Login
                            </span>

                            <label htmlFor="emailField" style={{marginBottom: '20px'}}>Email:</label>
                            <div  className="wrap-input100 validate-input" >
                                <input className="input100" id="emailField" name="email" placeholder="Email"  onChange={this.handleChange}/>
                                <span className="focus-input100"/>
                                <span className="symbol-input100">
							            <i className="fa fa-user-circle-o" aria-hidden="true"/>
						            </span>
                            </div>

                            <label htmlFor="passwordField" style={{marginBottom: '20px'}}>Password:</label>
                            <div   className="wrap-input100 validate-input" data-validate="Password is required">
                                <input className="input100" type="password" id="passwordField" name="password" placeholder="Password"  onChange={this.handleChange}/>
                                <span className="focus-input100"/>
                                <span className="symbol-input100">
							            <i className="fa fa-lock" aria-hidden="true"/>
						            </span>
                            </div>

                            <p className="error-message-login"  style={{display: this.state.showErrMsg ? 'block' : 'none' }}> ** User unauthorized </p>

                            <div className="container-login100-form-btn">
                                <button type="button" className="login100-form-btn" onClick={this.handleSubmit}>
                                    Login
                                </button>
                            </div>


                            <div className="text-center p-t-136">
                                <a className="txt2" href="/register">
                                    Create your Account
                                    <i className="fa fa-long-arrow-right m-l-5" aria-hidden="true"/>
                                </a>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        );
    }
}

export default LoginContainer;
